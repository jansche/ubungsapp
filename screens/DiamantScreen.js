import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  View,
  TouchableHighlight,
} from 'react-native';
import { Audio } from 'expo';

export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {text: ''};
    soundObject = new Audio.Sound();
    soundObject.loadAsync(require('../assets/sounds/diamond.mp3'));
  }
  static navigationOptions = {
    header: null,
  }; 
  
  render() {
      return (
          <View style={{backgroundColor: 'black', justifyContent: 'center', padding: 6}}>
            <TouchableHighlight onPress={this.onPress}>
              <Image
                source={ require('../assets/images/diamond.png')}
                resizeMode="contain"
              /> 
            </TouchableHighlight> 
      </View>
    );
  } 
  onPress = () => {
    soundObject.playAsync();
    soundObject.setPositionAsync(0);
  } 
}
