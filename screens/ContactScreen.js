import React from 'react';
import {
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Alert,
} from 'react-native';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Kontakte',
  };
  state = {
    contacts:[
      {
        vorname: "Petra",
        nachname: "Somschor",
        img: require('../assets/images/somschor.jpg')
      },
      {
        vorname: "Franz",
        nachname: "Fraisl",
        img: require('../assets/images/fraisl.jpg')
      },
      {
        vorname: "Thomas",
        nachname: "Mohr",
        img: require('../assets/images/mohr.jpg')
      },
      {
        vorname: "Miriam",
        nachname: "Filler",
        img: require('../assets/images/filler.png')
      },
      {
        vorname: "Günter",
        nachname: "Hämmerle",
        img: require('../assets/images/haemmerle.jpg')
      }
    ]
  }

  render() {
    return (
      <ScrollView style={{ margin: 10}}>
        {
          this.state.contacts.map((item, index) => (
            <TouchableOpacity
              key = {item.vorname}
              onPress = { () => Alert.alert(
                'Info',
                item.vorname + " " + item.nachname,
                [
                  {text: 'OK'},
                ],
                {cancelable: false},
              )}>
                <View style={{flexDirection:'row', marginTop: 10}}>
                  <Image source = {item.img} style={style.image}/>
                  <Text style={{ marginTop: 10, fontSize:15, marginLeft: 5}}> {item.vorname + " " + item.nachname}</Text>
                </View>
            </TouchableOpacity>
          )
          )
        }
      </ScrollView>
    );
  }
}

const style=
{
  image: 
  {
    width: 40,
    height: 40
  },
  view:
  {
    flexDirection: "row"
  }
};
