import React, { Component } from 'react';

import {
    ScrollView,
    TextInput,
    View,
    Button,
    KeyboardAvoidingView,
} from 'react-native';

export default class Login extends Component {
  static navigationOptions = {
    title: 'Login',
  }
  state = {
    Username: '',
    Password: '',
  }

  onLoginPress = () => {
    if (this.state.Username === 'Test' && this.state.Password === '123') {
      this.props.navigation.navigate('Tabs');
    }
  }

  onSubmitEditing = () => {
    this.refs.Password.focus();
  }

  render() {
    return (
      <ScrollView style={{ padding: 20 }}>
        <TextInput placeholder='Username' style={{ margin: 20 }} returnKeyType="next" onSubmitEditing={this.onSubmitEditing} onChangeText={(Username) => this.setState({Username})}/>
        <TextInput placeholder='Password' style={{ margin: 20 }} ref={'Password'} onSubmitEditing={this.onLoginPress} onChangeText={(Password) => this.setState({Password})}/>
        <View style={{ margin: 10 }} />
        <KeyboardAvoidingView behavior="padding">
          <Button 
            onPress={this.onLoginPress}
            title="Submit"
            ref={'Submit'} 
          />
        </KeyboardAvoidingView>
      </ScrollView> 
      );
  }
}
