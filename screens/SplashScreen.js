import React from 'react';
import { View, Image } from 'react-native';

export default class Splash extends React.Component {

  constructor(props) {
    super(props);
    setTimeout(() => { this.props.navigation.navigate('Login'); }, 3000);
  }

  render() {
    return (
      <View style={{ backgroundColor: '#2C2E3B'}}>
       <Image source={ require('../assets/images/jh.png')}/>
      </View>
    );
  }
}
