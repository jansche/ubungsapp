import React from 'react';
import { createSwitchNavigator, createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation';
import { Platform } from 'react-native';
import TabBarIcon from '../components/TabBarIcon';
import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreen';
import ContactScreen from '../screens/ContactScreen';
import DiamantScreen from '../screens/DiamantScreen';

const SplashStack = createStackNavigator({ Splash: SplashScreen });
const LoginStack = createStackNavigator({ Login: LoginScreen });
const ContactStack = createStackNavigator({ Contact: ContactScreen});
const DiamondStack = createStackNavigator({ Diamond: DiamantScreen});

ContactStack.navigationOptions = {
  tabBarLabel: 'Contacts',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
}; 

DiamondStack.navigationOptions = {
  tabBarLabel: 'Diamant',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};

const BottomTabNavigator = createBottomTabNavigator({
  ContactStack,
  DiamondStack,
});

const TabNavigatorStack = createStackNavigator({
  BottomTabNavigator: BottomTabNavigator
});

const SwitchNavigator = createSwitchNavigator(
  {
    Splash: SplashStack,
    Login: LoginStack,
    Tabs: TabNavigatorStack,
  },
  { 
    initialRouteName: 'Splash',
  }
);

const AppContainer = createAppContainer(SwitchNavigator, BottomTabNavigator);

export default AppContainer;
